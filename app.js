const express = require('express');
const app = express();
const authRoute = require('./routes/auth-routes');
const path = require('path');

// Render Engine
app.set('view engine','ejs');

app.use('/auth',authRoute);

app.get('/',(request,response) => {
    response.render('home');
});

// Serving static files, otherwise css will not work.
app.use(express.static(path.join(__dirname,'public')));

app.listen(3000,() => {
    console.log('Listening on port 3000');
})