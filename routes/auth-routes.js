const auth = require('express').Router();

auth.get('/login',(request,response) => {
    response.render('login');
});

// Logout
auth.get('/logout',(request,response) => {
    
});

// Authentication with google
auth.get('/google',(request,response) => {

});

module.exports = auth;